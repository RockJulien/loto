﻿using System.Collections.Generic;
using System.Text;

namespace LotoSimulator.DataModels
{
    /// <summary>
    /// Definition of the <see cref="Result"/> class.
    /// </summary>
    public class Result
    {
        #region Fields

        /// <summary>
        /// Stores the results numbers.
        /// </summary>
        private HashSet<int> mNumbers;

        /// <summary>
        /// Stores the results stars.
        /// </summary>
        private HashSet<int> mStars;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the number count.
        /// </summary>
        public int NumberCount
        {
            get
            {
                return this.mNumbers.Count;
            }
        }

        /// <summary>
        /// Gets the star count.
        /// </summary>
        public int StarCount
        {
            get
            {
                return this.mStars.Count;
            }
        }

        /// <summary>
        /// Gets the results numbers.
        /// </summary>
        public IEnumerable<int> Numbers
        {
            get
            {
                return this.mNumbers;
            }
        }

        /// <summary>
        /// Gets the results stars.
        /// </summary>
        public IEnumerable<int> Stars
        {
            get
            {
                return this.mStars;
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        public Result() :
        this( null, null )
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Result"/> class.
        /// </summary>
        /// <param name="pNumbers"></param>
        /// <param name="pStars"></param>
        public Result(IEnumerable<int> pNumbers, IEnumerable<int> pStars)
        {
            this.mStars = new HashSet<int>();
            this.mNumbers = new HashSet<int>();

            if ( pNumbers != null )
            {
                foreach (int lNumber in pNumbers)
                {
                    this.mNumbers.Add( lNumber );
                }
            }

            if ( pStars != null )
            {
                foreach (int lStar in pStars)
                {
                    this.mStars.Add( lStar );
                }
            }
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Adds a new number to the result.
        /// </summary>
        /// <param name="pNumber"></param>
        /// <returns>True if added, false otherwise.</returns>
        public bool Add(int pNumber)
        {
            return this.mNumbers.Add( pNumber );
        }

        /// <summary>
        /// Adds a new star to the result.
        /// </summary>
        /// <param name="pStar"></param>
        /// <returns>True if added, false otherwise.</returns>
        public bool AddStar(int pStar)
        {
            return this.mStars.Add( pStar );
        }

        /// <summary>
        /// Turns this instance into a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder lBuilder = new StringBuilder();
            foreach (var lNumber in this.mNumbers)
            {
                lBuilder.Append( string.Format( "{0} - ", lNumber ) );
            }

            lBuilder.Append( "( " );

            int lCounter = 0;
            foreach (var lStar in this.mStars)
            {
                lBuilder.Append( string.Format( "{0}", lStar ) );

                if ( lCounter < this.mStars.Count - 1 )
                {
                    lBuilder.Append( " - " );
                }

                lCounter++;
            }

            lBuilder.AppendLine( " )" );

            return lBuilder.ToString();
        }

        #endregion Methods
    }
}
