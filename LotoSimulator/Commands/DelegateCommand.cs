﻿using System;
using System.Windows.Input;

namespace LotoSimulator.Commands
{
    /// <summary>
    /// Definition of the <see cref="DelegateCommand"/> class.
    /// </summary>
    internal sealed class DelegateCommand : ICommand
    {
        #region Fields

        /// <summary>
        /// Stores the command action to execute.
        /// </summary>
        private readonly Action mExecute;

        /// <summary>
        /// Stores the function checking whether the action can execute or not.
        /// </summary>
        private readonly Func<bool> mCanExecute;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fired when the flag "CanExecute" change.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        #endregion Events

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="pExecute">The command action</param>
        public DelegateCommand(Action pExecute) : 
        this(pExecute, null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="pExecute">The command action.</param>
        /// <param name="pCanExecute">The function checking whether the command can execute or not.</param>
        public DelegateCommand(Action pExecute, Func<bool> pCanExecute)
        {
            if (pExecute == null)
            {
                throw new ArgumentNullException("pExecute");
            }

            this.mExecute = pExecute;
            this.mCanExecute = pCanExecute;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Notifies the can execute flag has changed.
        /// </summary>
        public void InvalidateCanExecuteChanged()
        {
            this.CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Executes the command action.
        /// </summary>
        /// <param name="pParameter"></param>
        public void Execute(object pParameter)
        {
            this.mExecute();
        }

        /// <summary>
        /// Checks whether the command can execute or not.
        /// </summary>
        /// <param name="pParameter"></param>
        /// <returns></returns>
        public bool CanExecute(object pParameter)
        {
            return this.mCanExecute == null || this.mCanExecute();
        }

        #endregion Methods
    }
}
