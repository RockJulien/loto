﻿using System.Windows;
using System.Windows.Threading;

namespace LotoSimulator
{
    /// <summary>
    /// Definition of the <see cref="App"/> class.
    /// </summary>
    public partial class App : Application
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            this.DispatcherUnhandledException += this.OnDispatcherUnhandledException;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Delegate called on unhandled exception in the app.
        /// </summary>
        /// <param name="pSender"></param>
        /// <param name="pEventArgs"></param>
        private void OnDispatcherUnhandledException(object pSender, DispatcherUnhandledExceptionEventArgs pEventArgs)
        {
            MessageBox.Show( pEventArgs.Exception.ToString(), "Unhandled exception occured", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK, MessageBoxOptions.None);
            pEventArgs.Handled = true;
        }

        #endregion Methods
    }
}
