﻿using LotoSimulator.Commands;
using LotoSimulator.DataModels;
using LotoSimulator.Parsers;
using LotoSimulator.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

namespace LotoSimulator.ViewModels
{
    /// <summary>
    /// Definition of the <see cref="MainWindowModel"/> class.
    /// </summary>
    internal sealed class MainWindowModel : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        /// <summary>
        /// Stores the constant euro million numbers input file(s) path.
        /// </summary>
        private const string EUROMILLION_NUMBERS_INPUT = @"Resources\Input\EuroMillion\Numbers";

        /// <summary>
        /// Stores the constant euro million stars input file(s) path.
        /// </summary>
        private const string EUROMILLION_STARS_INPUT = @"Resources\Input\EuroMillion\Stars";

        /// <summary>
        /// Stores the constant loto numbers input file(s) path.
        /// </summary>
        private const string LOTO_NUMBERS_INPUT = @"Resources\Input\Loto\Numbers";

        /// <summary>
        /// Stores the constant loto stars input file(s) path.
        /// </summary>
        private const string LOTO_STARS_INPUT = @"Resources\Input\Loto\Stars";

        /// <summary>
        /// Stores the constant euro million results output file path.
        /// </summary>
        private const string EUROMILLION_RESULT_OUTPUT = @"Resources\Output\EuroMillion";

        /// <summary>
        /// Stores the constant loto results output file path.
        /// </summary>
        private const string LOTO_RESULT_OUTPUT = @"Resources\Output\Loto";

        /// <summary>
        /// Stores the int randomizer.
        /// </summary>
        private static Random sRandomizer;

        /// <summary>
        /// Stores the flag indicating whether the generator is for loto or not.
        /// </summary>
        private bool mIsLoto;

        /// <summary>
        /// Stores the set of result of one generation.
        /// </summary>
        private List<Result> mResults;

        /// <summary>
        /// Stores the sorted count by number.
        /// </summary>
        private SortedDictionary<int, int> mCountByNumber;

        /// <summary>
        /// Stores the sorted number by count.
        /// </summary>
        private List<KeyValuePair<int, int>> mNumberByCount;

        /// <summary>
        /// Stores the sorted count by number.
        /// </summary>
        private SortedDictionary<int, int> mCountByStar;

        /// <summary>
        /// Stores the sorted stars by count.
        /// </summary>
        private List<KeyValuePair<int, int>> mStarByCount;

        /// <summary>
        /// Stores the constants property names.
        /// </summary>
        private const string DataPropertyName = "Data",
                             StartsDataPropertyName = "StarsData",
                             SheetsPropertyName = "Sheets",
                             RowsPropertyName = "Rows",
                             ColumnsPropertyName = "Columns",
                             IsLotoPropertyName = "IsLoto",
                             SelectedDataTableIndexPropertyName = "SelectedDataTableIndex",
                             SelectedStarsDataTableIndexPropertyName = "SelectedStarsDataTableIndex",
                             ResultCountPropertyName = "ResultCount",
                             ResultNumbersPropertyName = "ResultNumbers";

        /// <summary>
        /// Stores the first set of data from 1 to 50 (or 49 for the loto)
        /// </summary>
        private DataSet mData;

        /// <summary>
        /// Stores the stars data.
        /// </summary>
        private DataSet mStarsData;

        private readonly DelegateCommand giveResultsCommand;

        private string mSheets = Settings.Default.Feuilles.ToString();
        private string mRows = Settings.Default.Lignes.ToString();
        private string mColumns = Settings.Default.Colonnes.ToString();
        
        private int mResultCount = 1;
        private string mResultNumbers = string.Empty;

        //private bool openCreatedFile = Settings.Default.OpenCreatedFile;

        /// <summary>
        /// Stores the currently selected data table index.
        /// </summary>
        private int mSelectedDataTableIndex;

        /// <summary>
        /// Stores the currently selected stars data table index.
        /// </summary>
        private int mSelectedStarsDataTableIndex;

        #endregion Fields

        #region Events

        /// <summary>
        /// Event fored on property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Events

        #region Properties

        /// <summary>
        /// Gets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return "Loto";
            }
        }
        
        public DelegateCommand GiveResultsCommand
        {
            get
            {
                return giveResultsCommand;
            }
        }

        /// <summary>
        /// Gets the result count needed.
        /// </summary>
        public int ResultCount
        {
            get
            {
                return this.mResultCount;
            }
            set
            {
                this.SetValue( ResultCountPropertyName, ref this.mResultCount, value );
            }
        }

        /// <summary>
        /// Gets the result numbers as string
        /// </summary>
        public string ResultNumbers
        {
            get
            {
                return this.mResultNumbers;
            }
            private set
            {
                this.SetValue( ResultNumbersPropertyName, ref this.mResultNumbers, value );
            }
        }

        /// <summary>
        /// Gets or sets the flag indicating whether the generator is for loto or not.
        /// </summary>
        public bool IsLoto
        {
            get
            {
                return this.mIsLoto;
            }
            set
            {
                this.SetValue( IsLotoPropertyName, ref this.mIsLoto, value );
            }
        }

        /// <summary>
        /// Gets the set of data from 1 to 50
        /// </summary>
        public DataSet Data
        {
            get { return this.mData; }
            private set { this.SetValue(DataPropertyName, ref this.mData, value); }
        }

        /// <summary>
        /// Gets the stars data.
        /// </summary>
        public DataSet StarsData
        {
            get
            {
                return this.mStarsData;
            }
            private set
            {
                this.SetValue(StartsDataPropertyName, ref this.mStarsData, value);
            }
        }

        public string Sheets
        {
            get { return this.mSheets; }
            set { this.SetValue(SheetsPropertyName, ref this.mSheets, value); }
        }

        public string Rows
        {
            get { return this.mRows; }
            set { this.SetValue(RowsPropertyName, ref this.mRows, value); }
        }

        public string Columns
        {
            get { return this.mColumns; }
            set { this.SetValue(ColumnsPropertyName, ref this.mColumns, value); }
        }

        private int? SheetsCount
        {
            get { return ToPositiveIntegerOrNull(this.Sheets); }
        }

        private int? RowsCount
        {
            get { return ToPositiveIntegerOrNull(this.Rows); }
        }

        private int? ColumnsCount
        {
            get { return ToPositiveIntegerOrNull(this.Columns); }
        }

        /// <summary>
        /// Gets the numbers input file path.
        /// </summary>
        public string DataInputPath
        {
            get
            {
                if ( this.mIsLoto )
                {
                    return string.Format( @"{0}\..\{1}", Environment.CurrentDirectory, LOTO_NUMBERS_INPUT );
                }
                else
                {
                    return string.Format( @"{0}\..\{1}", Environment.CurrentDirectory, EUROMILLION_NUMBERS_INPUT );
                }
            }
        }

        /// <summary>
        /// Gets the stars input file path.
        /// </summary>
        public string StarsDataInputPath
        {
            get
            {
                if ( this.mIsLoto )
                {
                    return string.Format( @"{0}\..\{1}", Environment.CurrentDirectory, LOTO_STARS_INPUT );
                }
                else
                {
                    return string.Format( @"{0}\..\{1}", Environment.CurrentDirectory, EUROMILLION_STARS_INPUT );
                }
            }
        }

        /// <summary>
        /// Gets the results output file path.
        /// </summary>
        public string OutputPath
        {
            get
            {
                if ( this.mIsLoto )
                {
                    return string.Format( @"{0}\..\{1}", Environment.CurrentDirectory, LOTO_RESULT_OUTPUT );
                }
                else
                {
                    return string.Format( @"{0}\..\{1}", Environment.CurrentDirectory, EUROMILLION_RESULT_OUTPUT );
                }
            }
        }
        
        /// <summary>
        /// Gets or sets the currently selected index in the table.
        /// </summary>
        public int SelectedDataTableIndex
        {
            get { return this.mSelectedDataTableIndex; }
            set { this.SetValue(SelectedDataTableIndexPropertyName, ref this.mSelectedDataTableIndex, value); }
        }

        /// <summary>
        /// Gets or sets the currently selected index in the stars table.
        /// </summary>
        public int SelectedStarsDataTableIndex
        {
            get { return this.mSelectedStarsDataTableIndex; }
            set { this.SetValue(SelectedStarsDataTableIndexPropertyName, ref this.mSelectedStarsDataTableIndex, value); }
        }

        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string propertyName]
        {
            get
            {
                switch (propertyName)
                {
                    case SheetsPropertyName:
                        //this.CreateCommand.InvalidateCanExecuteChanged();
                        return this.SheetsCount.HasValue ? string.Empty : "Value must be a positive integer.";
                    case RowsPropertyName:
                        //this.CreateCommand.InvalidateCanExecuteChanged();
                        return this.RowsCount.HasValue ? string.Empty : "Value must be a positive integer.";
                    case ColumnsPropertyName:
                        //this.CreateCommand.InvalidateCanExecuteChanged();
                        return this.ColumnsCount.HasValue ? string.Empty : "Value must be a positive integer.";
                    //case InputPathPropertyName:
                    //    this.ReadCommand.InvalidateCanExecuteChanged();
                    //    return File.Exists(this.InputPath) ? string.Empty : "File doesn't exist.";
                    //case OutputPathPropertyName:
                    //    this.WriteCommand.InvalidateCanExecuteChanged();
                    //    return DirectoryExists(this.OutputPath) ? string.Empty : "Directory doesn't exist.";
                    default:
                        return string.Empty;
                }
            }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes static member(s) of the <see cref="MainWindowModel"/> class.
        /// </summary>
        static MainWindowModel()
        {
            sRandomizer = new Random();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowModel"/> class.
        /// </summary>
        public MainWindowModel()
        {
            this.mResults = new List<Result>();

            this.mCountByStar = new SortedDictionary<int, int>();
            this.mStarByCount = new List<KeyValuePair<int, int>>();

            this.mCountByNumber = new SortedDictionary<int, int>();
            this.mNumberByCount = new List<KeyValuePair<int, int>>();
            
            this.giveResultsCommand = new DelegateCommand(this.ComputeResults);

            this.Create();
            this.CreateStarsPannel();

            this.ReadNumbers();
            this.ReadStars();

            // Prepares caches.
            this.FillCaches();

            App.Current.Exit += this.OnExit;
        }

        #endregion Constructor

        #region Methods

        #region Methods Internal

        private static int? ToPositiveIntegerOrNull(string value)
        {
            int result;
            if (int.TryParse(value, out result) && result > 0)
                return result;
            else
                return null;
        }

        private static bool DirectoryExists(string path)
        {
            return string.IsNullOrWhiteSpace(path) ? false : Directory.Exists(Path.GetDirectoryName(Path.GetFullPath(path)));
        }

        private void SetValue<T>(string propertyName, ref T field, T value)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if ( e.PropertyName == "IsLoto" )
            {
                // Reset all.
                this.ReadNumbers();
                this.ReadStars();
                this.FillCaches();
            }

            var handler = this.PropertyChanged;
            if (handler != null)
                handler(this, e);
        }

        private void OnExit(object sender, ExitEventArgs pEventArgs)
        {
            var count = this.SheetsCount;
            if (count.HasValue)
                Settings.Default.Feuilles = count.Value;

            count = this.RowsCount;
            if (count.HasValue)
                Settings.Default.Lignes = count.Value;

            count = this.ColumnsCount;
            if (count.HasValue)
                Settings.Default.Colonnes = count.Value;
            
            Settings.Default.Save();
        }

        #endregion Methods Internal

        private bool CanCreate()
        {
            return this.SheetsCount.HasValue && this.RowsCount.HasValue && this.ColumnsCount.HasValue;
        }

        private void Create()
        {
            if (this.CanCreate())
            {
                var lData = new DataSet();

                for (int i = 0, sheetsCount = this.SheetsCount.Value; i < sheetsCount; ++i)
                {
                    var table = lData.Tables.Add(string.Format(CultureInfo.InvariantCulture, "Sheet {0}", i + 1));
                    int columnsCount = this.ColumnsCount.Value;
                    for (int j = 0; j < columnsCount; ++j)
                        table.Columns.Add(string.Format(CultureInfo.InvariantCulture, "Column {0}", j + 1), typeof(string));

                    for (int j = 0, rowsCount = this.RowsCount.Value; j < rowsCount; ++j)
                        table.Rows.Add(new string[columnsCount]);
                }

                this.Data = lData;
                this.SelectedDataTableIndex = 0;
            }
        }

        private void CreateStarsPannel()
        {
            if (this.CanCreate())
            {
                var lStarsData = new DataSet();

                for (int i = 0, sheetsCount = this.SheetsCount.Value; i < sheetsCount; ++i)
                {
                    var table = lStarsData.Tables.Add(string.Format(CultureInfo.InvariantCulture, "Sheet {0}", i + 1));
                    int columnsCount = this.ColumnsCount.Value;
                    for (int j = 0; j < columnsCount; ++j)
                        table.Columns.Add(string.Format(CultureInfo.InvariantCulture, "Column {0}", j + 1), typeof(string));

                    for (int j = 0, rowsCount = this.RowsCount.Value; j < rowsCount; ++j)
                        table.Rows.Add(new string[columnsCount]);
                }

                this.StarsData = lStarsData;
                this.SelectedStarsDataTableIndex = 0;
            }
        }
        
        /// <summary>
        /// Reads the numbers file(s).
        /// </summary>
        private void ReadNumbers()
        {
            if (this.CanCreate())
            {
                string[] lNumbersFiles = Directory.GetFiles( this.DataInputPath, "*.ods" );
                DataSet lData = new OdsReaderWriter().ReadOdsFile( lNumbersFiles.FirstOrDefault() );
                if ( lData != null )
                {
                    this.Data = lData;
                    this.SelectedDataTableIndex = 0;
                }
            }
        }

        /// <summary>
        /// Reads the stars file(s).
        /// </summary>
        private void ReadStars()
        {
            if (this.CanCreate())
            {
                string[] lStarsFiles = Directory.GetFiles( this.StarsDataInputPath, "*.ods" );
                DataSet lData = new OdsReaderWriter().ReadOdsFile( lStarsFiles.FirstOrDefault() );
                if ( lData != null )
                {
                    this.StarsData = lData;
                    this.SelectedStarsDataTableIndex = 0;
                }
            }
        }

        private bool CanWrite()
        {
            return this.Data != null && DirectoryExists(this.OutputPath);
        }

        /// <summary>
        /// Writes the results to file.
        /// </summary>
        /// <returns>The merged result(s) as string.</returns>
        private string Write()
        {
            if (this.CanWrite())
            {
                DateTime lTime = DateTime.Now;
                string lFileName = string.Format( "{0}_{1}_{2}_{3}_{4}_{5}_Result", lTime.Year, lTime.Month, lTime.Day, lTime.Hour, lTime.Minute, lTime.Second );

                string lOutputFilePath = string.Format( @"{0}\{1}", this.OutputPath, lFileName );

                StringBuilder lBuilder = new StringBuilder();
                foreach ( Result lResult in this.mResults )
                {
                    lBuilder.AppendLine( lResult.ToString() );
                }

                string lMergedResults = lBuilder.ToString();

                File.WriteAllText( lOutputFilePath, lMergedResults );

                return lMergedResults;
            }

            return string.Empty;
        }
        
        /// <summary>
        /// Compute the results.
        /// </summary>
        private void ComputeResults()
        {
            try
            {
                this.mResults.Clear();
                
                int lNumberCount = 5;
                int lStarCount = 2;
                if ( this.mIsLoto )
                {
                    lStarCount = 1;
                }

                List<KeyValuePair<int, int>> lOrderedNumberList = this.mNumberByCount.OrderByDescending( pElt => pElt.Key ).ToList();
                List<KeyValuePair<int, int>> lOrderedStarList   = this.mStarByCount.OrderByDescending( pElt => pElt.Key ).ToList();
                for (int lCurr = 0; lCurr < this.ResultCount; lCurr++)
                {
                    Result lResult = GiveSet( lOrderedNumberList, lOrderedStarList, (ResultWeight)lCurr, lNumberCount, lStarCount );
                    this.mResults.Add( lResult );
                }


                this.ResultNumbers = this.Write();
            }
            catch
            {
                this.ResultNumbers = "Vérifier les tables !!!";
            }
        }

        /// <summary>
        /// Fills the caches.
        /// </summary>
        private void FillCaches()
        {
            this.mCountByNumber.Clear();
            this.mNumberByCount.Clear();

            int lColumnCount = 50;
            if ( this.mIsLoto )
            {
                lColumnCount = 49;
            }

            // Process numbers table.
            foreach (DataTable lTable in this.mData.Tables)
            {
                if ( lTable.Columns.Count < lColumnCount )
                {
                    continue;
                }

                for ( int lCurrNumber = 2; lCurrNumber <= lColumnCount + 1; lCurrNumber++ )
                {
                    string lColumnName = "Column " + lCurrNumber.ToString();
                    int lOldCount = 0;
                    int lCount = 0;
                    int lRowCounter = 0;
                    foreach ( DataRow lRow in lTable.Rows )
                    {
                        // Skip the first row.
                        if( lRowCounter == 0 )
                        {
                            lRowCounter++;
                            continue;
                        }

                        object lValue = lRow[ lColumnName ];
                        if ( lValue != null &&
                                lValue is System.DBNull == false )
                        {
                            int lResultValue;
                            if( int.TryParse( lValue as string, out lResultValue ) )
                            {
                                lCount += lResultValue;
                            }
                        }

                        lRowCounter++;
                    }
                    
                    if ( this.mCountByNumber.TryGetValue( lCurrNumber - 1, out lOldCount ) )
                    {
                        lCount += lOldCount;
                    }

                    this.mCountByNumber[ lCurrNumber - 1 ] = lCount;
                }
            }

            foreach (var lPair in this.mCountByNumber)
            {
                this.mNumberByCount.Add( new KeyValuePair<int, int>( lPair.Value, lPair.Key ) );
            }

            this.mCountByStar.Clear();
            this.mStarByCount.Clear();

            lColumnCount = 12;
            if ( this.mIsLoto )
            {
                lColumnCount = 10;
            }

            // Process stars table.
            foreach (DataTable lTable in this.mStarsData.Tables)
            {
                if ( lTable.Columns.Count < lColumnCount )
                {
                    continue;
                }

                for ( int lCurrNumber = 2; lCurrNumber <= lColumnCount + 1; lCurrNumber++ )
                {
                    string lColumnName = "Column " + lCurrNumber.ToString();
                    int lOldCount = 0;
                    int lCount = 0;
                    int lRowCounter = 0;
                    foreach ( DataRow lRow in lTable.Rows )
                    {
                        // Skip the first row.
                        if( lRowCounter == 0 )
                        {
                            lRowCounter++;
                            continue;
                        }

                        object lValue = lRow[ lColumnName ];
                        if ( lValue != null &&
                                lValue is System.DBNull == false )
                        {
                            int lResultValue;
                            if( int.TryParse( lValue as string, out lResultValue ) )
                            {
                                lCount += lResultValue;
                            }
                        }

                        lRowCounter++;
                    }
                    
                    if ( this.mCountByStar.TryGetValue( lCurrNumber - 1, out lOldCount ) )
                    {
                        lCount += lOldCount;
                    }

                    this.mCountByStar[ lCurrNumber - 1 ] = lCount;
                }
            }

            foreach (var lPair in this.mCountByStar)
            {
                this.mStarByCount.Add( new KeyValuePair<int, int>( lPair.Value, lPair.Key ) );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSortedNumbers"></param>
        /// <param name="pSortedStars"></param>
        /// <param name="pWeight"></param>
        /// <param name="pNumberCount"></param>
        /// <param name="pStarCount"></param>
        /// <returns></returns>
        private static Result GiveSet(List<KeyValuePair<int, int>> pSortedNumbers, List<KeyValuePair<int, int>> pSortedStars, ResultWeight pWeight, int pNumberCount, int pStarCount)
        {
            switch (pWeight)
            {
                case ResultWeight.Higher:
                    return GiveHigher( pSortedNumbers, pSortedStars, pNumberCount, pStarCount );
                case ResultWeight.Lower:
                    return GiveLower( pSortedNumbers, pSortedStars, pNumberCount, pStarCount );
                case ResultWeight.Average:
                    return GiveAverage( pSortedNumbers, pSortedStars, pNumberCount, pStarCount );
                case ResultWeight.Random:
                    return GiveRandom( pSortedNumbers, pSortedStars, pNumberCount, pStarCount );
                default:
                    return GiveRandom( pSortedNumbers, pSortedStars, pNumberCount, pStarCount );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSortedNumbers"></param>
        /// <param name="pSortedStars"></param>
        /// <param name="pNumberCount"></param>
        /// <param name="pStarCount"></param>
        /// <returns></returns>
        private static Result GiveHigher(List<KeyValuePair<int, int>> pSortedNumbers, List<KeyValuePair<int, int>> pSortedStars, int pNumberCount, int pStarCount)
        {
            Result lResult = new Result();
            if ( pSortedNumbers.Count < pNumberCount )
            {
                return lResult;
            }

            for (int lCurr = 0; lCurr < pNumberCount; lCurr++)
            {
                lResult.Add( pSortedNumbers[ lCurr ].Value );
            }

            for (int lCurr = 0; lCurr < pStarCount; lCurr++)
            {
                lResult.AddStar( pSortedStars[ lCurr ].Value );
            }

            return lResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSortedNumbers"></param>
        /// <param name="pSortedStars"></param>
        /// <param name="pNumberCount"></param>
        /// <param name="pStarCount"></param>
        /// <returns></returns>
        private static Result GiveLower(List<KeyValuePair<int, int>> pSortedNumbers, List<KeyValuePair<int, int>> pSortedStars, int pNumberCount, int pStarCount)
        {
            Result lResult = new Result();
            if ( pSortedNumbers.Count < pNumberCount )
            {
                return lResult;
            }

            int lLastIndex = pSortedNumbers.Count - 1;
            for (int lCurr = lLastIndex; lCurr > lLastIndex - pNumberCount; lCurr--)
            {
                lResult.Add( pSortedNumbers[ lCurr ].Value );
            }

            int lLastStarIndex = pSortedStars.Count - 1;
            for (int lCurr = lLastStarIndex; lCurr > lLastStarIndex - pStarCount; lCurr--)
            {
                lResult.AddStar( pSortedStars[ lCurr ].Value );
            }

            return lResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSortedNumbers"></param>
        /// <param name="pSortedStars"></param>
        /// <param name="pNumberCount"></param>
        /// <param name="pStarCount"></param>
        /// <returns></returns>
        private static Result GiveAverage(List<KeyValuePair<int, int>> pSortedNumbers, List<KeyValuePair<int, int>> pSortedStars, int pNumberCount, int pStarCount)
        {
            Result lResult = new Result();

            int lHalfIndex = pSortedNumbers.Count / 2;
            int lHalfStarIndex = pSortedStars.Count / 2;
            if ( pSortedNumbers.Count < lHalfIndex + pNumberCount ||
                 pSortedStars.Count < lHalfStarIndex + pStarCount )
            {
                return lResult;
            }

            for (int lCurr = lHalfIndex; lCurr < lHalfIndex + pNumberCount; lCurr++)
            {
                lResult.Add( pSortedNumbers[ lCurr ].Value );
            }

            for (int lCurr = lHalfStarIndex; lCurr < lHalfStarIndex + pStarCount; lCurr++)
            {
                lResult.AddStar( pSortedStars[ lCurr ].Value );
            }

            return lResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pSortedNumbers"></param>
        /// <param name="pSortedStars"></param>
        /// <param name="pNumberCount"></param>
        /// <param name="pStarCount"></param>
        /// <returns></returns>
        private static Result GiveRandom(List<KeyValuePair<int, int>> pSortedNumbers, List<KeyValuePair<int, int>> pSortedStars, int pNumberCount, int pStarCount)
        {
            Result lResult = new Result();
            if ( pSortedNumbers.Count < pNumberCount ||
                 pSortedStars.Count < pStarCount )
            {
                return lResult;
            }

            if ( pSortedNumbers.Count - 1 < 0 ||
                 pSortedStars.Count - 1 < 0 )
            {
                return lResult;
            }

            // Avoid duplicates due to randomizer.
            while ( lResult.NumberCount < pNumberCount )
            {
                int lRandomIndex = sRandomizer.Next( 0, pSortedNumbers.Count - 1 );
                lResult.Add( pSortedNumbers[ lRandomIndex ].Value );
            }

            // Avoid duplicates due to randomizer.
            while ( lResult.StarCount < pStarCount )
            {
                int lRandomIndex = sRandomizer.Next( 0, pSortedStars.Count - 1 );
                lResult.AddStar( pSortedStars[ lRandomIndex ].Value );
            }

            return lResult;
        }

        /// <summary>
        /// Definition of the <see cref="ResultWeight"/> enum.
        /// </summary>
        public enum ResultWeight
        {
            /// <summary>
            /// None
            /// </summary>
            None = 0,

            /// <summary>
            /// Highter
            /// </summary>
            Higher = 1,

            /// <summary>
            /// Lower
            /// </summary>
            Lower = 2,

            /// <summary>
            /// Average
            /// </summary>
            Average = 3,

            /// <summary>
            /// Random
            /// </summary>
            Random,

            /// <summary>
            /// The weight count.
            /// </summary>
            Count
        }

        #endregion Methods
    }
}
